import logo from './foxhound.png';
import './App.css';

function App(){
  return(
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        <p>
          Compañia familiar Fox Hound
        </p>
        <a 
          className="App-link"
          href="http://es.wikipedia.org/wiki/FOXHOUND"
          target="_blank"
          rel="noopener noreferrer" >
        Learn more...
        </a>
      </header>
    </div>
  );
}


export default App;
